# -*- coding: utf-8 -*-
# Copyright (2022) Cardiff University
# Licensed under GPLv3+ - see LICENSE

"""GWDataFind API
"""

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

# by default support the v1 API and the LDR compatibility API
DEFAULT_API = ["v1", "ldr"]
