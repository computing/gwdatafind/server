# GWDataFind Server

This module defines a Flask Application that serves data URLs based on the contents of a diskcache.
A link to the documentation page can be found [here](https://computing.docs.ligo.org/gwdatafind/server/index.html).

### Requirements

* Python >= 3.4
* Flask >= 1.0.0

## Apache + Gunicorn configuration

For a detailed description of how to set up and configure a datafind server, 
please see [here](https://computing.docs.ligo.org/gwdatafind/server/configuration.html).

## Client API

A detailed explination of the API can be found [here](https://computing.docs.ligo.org/gwdatafind/server/api/v1.html).
