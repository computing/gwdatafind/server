# -*- coding: utf-8 -*-
# Copyright (2022) Cardiff University
# Licensed under GPLv3+ - see LICENSE

__author__ = "Duncan Macleod <duncan.macleod@ligo.org>"

from setuptools import setup

setup(use_scm_version=True)
